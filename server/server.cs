﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Linq;

namespace server
{
    class Program
    {
        class Player{
            public string name;
            public int port;
            public bool ready = false;
            //public int[] flagLocations;
            public string flagLocations;
            public Socket sock;
            public Player Opponent;
        }
        static void Main(string[] args)
        {
            Player player1 = new Player();
            Player player2 = new Player();
            player1.Opponent = player2;
            player2.Opponent = player1;

            byte[] data = new byte[1024];
            int recv;
            string Connected = " Connected";

            ArrayList sockList = new ArrayList(2);
            ArrayList copyList = new ArrayList(2);

            Console.WriteLine("Starting server...");
            IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, 8000);

            //create the TCP socket
            Socket sock = new Socket(AddressFamily.InterNetwork,
                            SocketType.Stream, ProtocolType.Tcp);

            //bind the socket to the new IPEndPoint object and listen for incoming connections.
            sock.Bind(localEndPoint);
            sock.Listen(2);

            //accept an incoming connection attempt from a client
            Socket client1 = sock.Accept();
            IPEndPoint iep = (IPEndPoint) client1.RemoteEndPoint;
            Console.WriteLine("Connected with {0} at port {1}", iep.Address, iep.Port);
            client1.Send(Encoding.ASCII.GetBytes(Connected),Connected.Length,SocketFlags.None);
            sockList.Add(client1);
            recv = client1.Receive(data);
            if(data[0] == 0x24) player1.name = Encoding.ASCII.GetString(data, 1, recv-1);
            player1.port = iep.Port;
            player1.sock = client1;
            Console.WriteLine("Name: {0}", player1.name);

            
            Socket client2 = sock.Accept();
            IPEndPoint iep2 = (IPEndPoint)client2.RemoteEndPoint;
            Console.WriteLine("Connected to {0}", iep2.ToString());
            client2.Send(Encoding.ASCII.GetBytes(Connected),Connected.Length,SocketFlags.None);
            sockList.Add(client2);
            recv = client2.Receive(data);
            if(data[0] == 0x24) player2.name = Encoding.ASCII.GetString(data, 1, recv-1);
            player2.port = iep2.Port;
            player2.sock = client2;
            Console.WriteLine("Name: {0}", player2.name);

            //Send each other the player names
            client1.Send(Encoding.ASCII.GetBytes("$" + player2.name), player2.name.Length + 1, SocketFlags.None);
            client2.Send(Encoding.ASCII.GetBytes("$" + player1.name), player1.name.Length + 1, SocketFlags.None);

            bool flag = false;
            string stringData;
            byte header;
            while(true)
            {
                copyList = new ArrayList(sockList);
                Socket.Select(copyList, null, null, 2000000);
                foreach(Socket client in copyList)
                {
                    try
                    {  
                        data = new byte[1024];
                        recv = client.Receive(data);
                        header = data[0];
                        stringData = Encoding.ASCII.GetString(data, 1, recv-1);
                        Console.WriteLine("Received: {0}, {1}",header.ToString(), stringData);
                        ExecuteSelection(header, stringData, ((IPEndPoint)(client.RemoteEndPoint)).Port == player1.port ? player1 : player2);
                    }
                    catch(SocketException)
                    {
                        iep = (IPEndPoint)client.RemoteEndPoint;
                        Console.WriteLine("Client {0} disconnected.", iep.ToString());
                        flag = true;
                        client.Close();
                        sockList.Remove(client);
                        if (sockList.Count == 0)
                        {
                            Console.WriteLine("Last client disconnected, bye");
                            return;
                        }
                    }
                }

                if(flag)
                {
                    Socket remaining = (Socket) sockList[0];
                    remaining.Send(Encoding.ASCII.GetBytes("!Opponent disconnected"));
                    flag = false;
                }
            }

        }

        static void ExecuteSelection(byte s, string data, Player player)
        {
            if(s == 0x25)
            {
                player.flagLocations = data;
                player.ready = true;
                if(player.Opponent.ready) StartGame(player);
            }
            else
                player.Opponent.sock.Send( addByteToStart(Encoding.ASCII.GetBytes(data),s), data.Length+1, SocketFlags.None);
        }

        static void StartGame(Player player)
        {
            Console.WriteLine("The game is on!");

            player.sock.Send(addByteToStart(Encoding.ASCII.GetBytes(player.Opponent.flagLocations),0x23),player.Opponent.flagLocations.Length +1, SocketFlags.None);
            player.Opponent.sock.Send(addByteToStart(Encoding.ASCII.GetBytes(player.flagLocations),0x22),player.flagLocations.Length +1, SocketFlags.None);
        }

        public static byte[] addByteToStart(byte[] bArray, byte newByte)
        {
            byte[] newArray = new byte[bArray.Length + 1];
            bArray.CopyTo(newArray, 1);
            newArray[0] = newByte;
            return newArray;
        }
    }
}
