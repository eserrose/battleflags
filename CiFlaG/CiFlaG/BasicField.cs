﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CiFlaG
{
    class BasicField : IField
    {
        public bool Attacked { get; set;}
        public bool Defended { get; set;}
        public virtual void Draw(Graphics g, Point p, Size sz)
        {
            g.DrawRectangle(Pens.Black, new Rectangle(p.X, p.Y, sz.Width - 1, sz.Height - 1));
            if (Attacked)
            {
                g.FillEllipse(Brushes.Black, p.X + sz.Width / 2 - 4, p.Y + sz.Width / 2 - 4, 8, 8);
            }
            if (Defended)
            {
                g.DrawLine(Pens.Black, p.X + 5, p.Y + 5, p.X + sz.Width - 5, p.Y + sz.Height - 5);
                g.DrawLine(Pens.Black, p.X + sz.Width - 5, p.Y + 5, p.X + 5, p.Y + sz.Height - 5);
            }
        }
    }
}
