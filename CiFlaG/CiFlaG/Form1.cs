﻿#define DEBUG
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;

namespace CiFlaG
{
 
    public partial class Form1 : Form
    {
        Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        byte[] data = new byte[1024];
        bool Ready = false;

        private GameController gameController;
        string player_name = "Player 1", opponent_name = "Player 2";
        public enum Commands
        {
            CMD_CHAT_ID = 0x1F,
            CMD_INFO_ID,
            CMD_ERROR_ID,
            CMD_START_1_ID,
            CMD_START_2_ID,
            CMD_SET_NAME,
            CMD_FLAG_LOC,
            CMD_ATTACK_ID,
            CMD_TURN_ID
        }

        public Form1()
        {
            InitializeComponent();
        }

        private bool StartConnection(string ipAdd, Int32 Port )
        {
            IPEndPoint ipep = new IPEndPoint(IPAddress.Parse(ipAdd), Port); //127.0.0.1
            server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                server.BeginConnect(ipep, new AsyncCallback(Connected), server);
            }
            catch (SocketException e)
            {
                Console.WriteLine("Unable to connect to server.");
                return false;
            }

            return true;
        }

        void Connected(IAsyncResult iar)
        {
            server = (Socket)iar.AsyncState;
            try
            {
                server.EndConnect(iar);
                server.BeginReceive(data, 0, 1024, SocketFlags.None, new AsyncCallback(ReceiveData), server);
            }
            catch (SocketException)
            {
                MessageBox.Show("Unable to Connect");
            }
        }

        private void EndConnection()
        {
            Console.WriteLine("Disconnected");
            button2.Enabled = false;
            server.Close();
        }

        void SendData(IAsyncResult iar)
        {
            Socket remote = (Socket)iar.AsyncState;
            int sent = remote.EndSend(iar);
            remote.BeginReceive(data, 0, 1024, SocketFlags.None,new AsyncCallback(ReceiveData), remote);
        }

        void ReceiveData(IAsyncResult iar)
        {
            Socket remote = (Socket)iar.AsyncState;
            int recv = remote.EndReceive(iar);
            switch (data[0])
            {
                case (byte)Commands.CMD_CHAT_ID:
                    SetText( opponent_name + ": " + Encoding.ASCII.GetString(data, 1, recv - 1));
                    break;
                case (byte)Commands.CMD_INFO_ID:
                    SetText("INFO:" + Encoding.ASCII.GetString(data, 1, recv - 1));
                    break;
                case (byte)Commands.CMD_ERROR_ID:
                    SetText("ERROR:" + Encoding.ASCII.GetString(data, 1, recv - 1));
                    break;
                case (byte)Commands.CMD_START_1_ID:
                    Ready = true;
                    ControlInvokeRequired(logBox, () => logBox.Items.Add(player_name + " goes first."));
                    gameController.SetOpponentFlags(Encoding.ASCII.GetString(data, 1, recv - 1).Split(',').Select(Int32.Parse).ToList());
                    break;
                case (byte)Commands.CMD_START_2_ID:
                    Ready = false;
                    ControlInvokeRequired(logBox, () => logBox.Items.Add(opponent_name + " goes first."));
                    gameController.SetOpponentFlags(Encoding.ASCII.GetString(data, 1, recv - 1).Split(',').Select(Int32.Parse).ToList());
                    break;
                case (byte)Commands.CMD_SET_NAME:
                    if(recv > 1)
                        opponent_name = Encoding.ASCII.GetString(data, 1, recv - 1);
                    ControlInvokeRequired(radioButton2, () => radioButton2.Checked = true);
                    ControlInvokeRequired(StartGameBtn, () => StartGameBtn.Enabled = true);
                    ControlInvokeRequired(chatBox, () => chatBox.Enabled = true);
                    break;
                case (byte)Commands.CMD_FLAG_LOC:
                    gameController.SetOpponentFlags(Encoding.ASCII.GetString(data, 1, recv - 1).Split(',').Select(Int32.Parse).ToList());
                    break;
                case (byte)Commands.CMD_ATTACK_ID:
                    Ready = true;
                    ControlInvokeRequired(logBox, () => logBox.Items.Add(player_name + " is on turn."));
                    gameController.DefendPoint(Encoding.ASCII.GetString(data, 1, recv - 1).Split(',').Select(Int32.Parse).ToList());
                    UpdateMap();
                    break;
                case (byte)Commands.CMD_TURN_ID:
                    ControlInvokeRequired(logBox, () => logBox.Items.Add(Encoding.ASCII.GetString(data, 1, recv - 1)));
                    break;
            }

            remote.BeginReceive(data, 0, 1024, SocketFlags.None, new AsyncCallback(ReceiveData), remote);
        }

        delegate void SetTextCallback(string text);
        private void SetText(string recv)
        {
            if (chatHistory.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { recv });
            }
            else
            {
                chatHistory.Items.Add("[" + DateTime.Now.ToLongTimeString() + "]" + recv);
                chatHistory.SelectedIndex = chatHistory.Items.Count - 1;
                chatHistory.SelectedIndex = -1;
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            string ipAdd = ipBox.Text;
            Int32 port = Int32.Parse(portBox.Text);

#if (DEBUG)
            Console.WriteLine("Starting Connection...");
#endif
            if (StartConnection(ipAdd, port))
            {
#if (DEBUG)
                Console.WriteLine("Success");
#endif
                button2.Enabled = true;
                radioButton1.Checked = true;
                button1.Enabled = false;

                while (!server.Connected); //Wait until connection is established

                byte[] toSend = { (byte) Commands.CMD_SET_NAME };
                if (nameBox.Text.Length > 0)
                {
                    player_name = nameBox.Text;
                    toSend = addByteToStart(Encoding.ASCII.GetBytes(player_name), (byte)Commands.CMD_SET_NAME);
                }
                server.BeginSend(toSend, 0, toSend.Length, SocketFlags.None, new AsyncCallback(SendData), server);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            String dtn = dt.ToShortTimeString();

            string input = chatBox.Text;
            byte[] toSend = addByteToStart(Encoding.ASCII.GetBytes(input), (byte) Commands.CMD_CHAT_ID);
            server.BeginSend(toSend, 0, toSend.Length, SocketFlags.None, new AsyncCallback(SendData), server);

            chatHistory.Items.Add("[" + DateTime.Now.ToLongTimeString() + "]" + player_name + ": " + input);
            chatHistory.SelectedIndex = chatHistory.Items.Count - 1;
            chatHistory.SelectedIndex = -1;

            chatBox.Text = "";
        }

        private void chatBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 && chatBox.Text.Length > 0)
            {
                button2.PerformClick();
                e.Handled = true;
            }
        }

        public byte[] addByteToStart(byte[] bArray, byte newByte)
        {
            byte[] newArray = new byte[bArray.Length + 1];
            bArray.CopyTo(newArray, 1);
            newArray[0] = newByte;
            return newArray;
        }

        private void StartGameBtn_Click(object sender, EventArgs e)
        {
            gameController = new GameController(player_name, opponent_name);
            logBox.Items.Add("Select 5 flags and click Ready");
            mapImg.Image = gameController.GetImage(mapImg.Size);
            gameController.DispInfo1 += gameController_DispInfo1;
            gameController.DispInfo2 += gameController_DispInfo2;
            gameController.PlayerOnTurn += gameController_PlayerOnTurn;
            gameController.PlayerWon += gameController_PlayerWon;
        }

        void gameController_PlayerWon(IPlayer obj)
        {
            MessageBox.Show(obj.Name + " won!");
            Application.Exit();
        }

        void gameController_PlayerOnTurn(string obj)
        {
            if (ControlInvokeRequired(logBox, () => logBox.Items.Add(obj))) return;
            else logBox.Items.Add(obj);
        }

        void gameController_DispInfo1(string obj)
        {
            byte[] toSend = addByteToStart(Encoding.ASCII.GetBytes(obj), (byte)Commands.CMD_TURN_ID);
            server.BeginSend(toSend, 0, toSend.Length, SocketFlags.None, new AsyncCallback(SendData), server);
        }

        void gameController_DispInfo2(string obj)
        {
            if (ControlInvokeRequired(logBox, () => logBox.Items.Add(obj))) return;
            else logBox.Items.Add(obj);
        }

        private void readyBtn_Click(object sender, EventArgs e)
        {
            string flagLocations = string.Join(",", gameController.SelectedFlags.ToArray());
            byte[] toSend = addByteToStart(Encoding.ASCII.GetBytes(flagLocations), (byte)Commands.CMD_FLAG_LOC);
            server.BeginSend(toSend, 0, toSend.Length, SocketFlags.None, new AsyncCallback(SendData), server);
        }

        private void mapImg_MouseClick(object sender, MouseEventArgs e)
        {
            if (gameController.count < 5)
            {
                gameController.SetUpFlags(e.Location.CalculatePointToBoard(mapImg.Size, gameController.x, gameController.y));
                if (gameController.count == 5) readyBtn.Enabled = true;
            }
            else if(Ready)
            {
                Ready = false;
                Point AttackPoint = e.Location.CalculatePointToBoard(mapImg.Size, gameController.x, gameController.y);
                gameController.Turn(AttackPoint);
                byte[] toSend = addByteToStart(Encoding.ASCII.GetBytes((AttackPoint.X).ToString() + "," + (AttackPoint.Y).ToString()), (byte)Commands.CMD_ATTACK_ID);
                server.BeginSend(toSend, 0, toSend.Length, SocketFlags.None, new AsyncCallback(SendData), server);
            }
            UpdateMap();
        }

        private void UpdateMap()
        {
            mapImg.Image = gameController.GetImage(mapImg.Size);
        }

        /// <summary>
        /// Helper method to determin if invoke required, if so will rerun method on correct thread.
        /// if not do nothing.
        /// </summary>
        /// <param name="c">Control that might require invoking</param>
        /// <param name="a">action to preform on control thread if so.</param>
        /// <returns>true if invoke required</returns>
        public bool ControlInvokeRequired(Control c, Action a)
        {
            if (c.InvokeRequired) c.Invoke(new MethodInvoker(delegate { a(); }));
            else return false;

            return true;
        }

    }
}
