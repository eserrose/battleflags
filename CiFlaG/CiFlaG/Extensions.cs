﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CiFlaG
{
    public static class Extensions
    {
        public static Point CalculatePointToBoard(this Point mousePosition, Size sz, int boardX, int boardY)
        {
            return new Point(mousePosition.X / (sz.Width / boardX), mousePosition.Y / (sz.Height / boardY));
        }
    }
}
