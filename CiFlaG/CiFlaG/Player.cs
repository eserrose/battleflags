﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CiFlaG
{
    class Player : IPlayer
    {
        public event Action<string> DispInfo = delegate { };

        public string Name { get; private set;}
        public GameBoard GameBoard { get; set;}

        public Player(string name, int x, int y)
        {
            this.Name = name;
            GameBoard = new GameBoard(x,y);
        }

        public bool Attack(int x, int y, IPlayer opponent)
        {
            opponent.Defend(x, y);
            if (!GameBoard.IsAttacked(x, y))
            {
                GameBoard[x, y].Attacked = true;
                if (opponent.GameBoard[x, y] is IFlag)
                    DispInfo(string.Format("Attack to {0} hit!", x));
                else
                    DispInfo(string.Format("Attack to {0} missed.", x));
                return true;
            }
            else
            {
                DispInfo(string.Format("You can't attack {0} twice", x));
                return false;
            }
        }

        public bool Defend(int x, int y)
        {
            if (!GameBoard.IsDefended(x, y))
            {
                GameBoard[x, y].Defended = true;
                if (GameBoard[x, y] is IFlag)
                    DispInfo(string.Format("Flag at {0} is hit!", x));
                else
                    DispInfo(string.Format("Miss", x));
                return true;
            }
            else
            {
                DispInfo(string.Format("{0} is already defended", x));
                return false;
            }
        }

        public bool HasFlags()
        {
            foreach(var item in GameBoard)
            {
                if (item is IFlag && !(item as IField).Defended)
                    return true;
            }
            return false;
        }
    }
}
