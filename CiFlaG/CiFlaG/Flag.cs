﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CiFlaG
{
    class Flag : BasicField, IFlag
    {
        public Color Color
        {
            get { return Color.DarkRed; }
        }

        public override void Draw(Graphics g, Point p, Size sz)
        {
            g.FillRectangle(new SolidBrush(Color), new Rectangle(p.X + 1, p.Y + 1, sz.Width - 2, sz.Height - 2));
            base.Draw(g, p, sz);
        }
    }
}
