﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CiFlaG
{
    class GameController
    {
        public readonly int x = Properties.Resources.map.Size.Width / 15;
        public readonly int y = Properties.Resources.map.Size.Height / 15;

        public int count = 0;
        public List<int> SelectedFlags = new List<int>();

        public event Action<string> DispInfo1 = delegate { };
        public event Action<string> DispInfo2 = delegate { };
        public event Action<string> PlayerOnTurn = delegate { };
        public event Action<IPlayer> PlayerWon = delegate { };

        private IPlayer player1;
        private IPlayer player2;

        private IPlayer playerForAttack;
        private IPlayer playerForDefend;

        public GameController(string player1, string player2)
        {
            this.player1 = new Player(player1, x, y);
            this.player2 = new Player(player2, x, y);
            this.player1.DispInfo += player1_DispInfo;
            this.player2.DispInfo += player2_DispInfo;
            playerForAttack = this.player1;
            playerForDefend = this.player2;
        }

        void player1_DispInfo(string obj)
        {
            DispInfo2(player2.Name + ": " + obj);
        }

        void player2_DispInfo(string obj)
        {
            DispInfo1(player1.Name + ": " + obj);
        }

        public Image GetImage(Size sz)
        {
            Image image = Properties.Resources.map;
            int w = 15;
            int h = 15;
            using (Graphics g = Graphics.FromImage(image))
            {
                for (int x = 0; x < playerForAttack.GameBoard.GetMaxX; x++)
                    for (int y = 0; y < playerForAttack.GameBoard.GetMaxY; y++)
                        playerForAttack.GameBoard[x, y].Draw(g, new Point(x * w, y * h), new Size(w, h));
            }
            return image;
        }

        internal void SetUpFlags(Point p)
        {
            if (!player1.GameBoard.IsFlag(p.X, p.Y))
            {
                count++;
                player1.GameBoard.AddFlag(p.X, p.Y);
                SelectedFlags.Add(p.X);
                SelectedFlags.Add(p.Y);
            }   
        }

        internal void SetOpponentFlags(List<int> l)
        {
            for (int i = 0; i < l.Count; i+=2)
            {
                player2.GameBoard.AddFlag(l[i], l[i+1]);
            }
            
        }

        internal void Turn(Point p)
        {
            if (playerForAttack.Attack(p.X, p.Y, playerForDefend))
            {
                if (!playerForDefend.HasFlags())
                    PlayerWon(playerForAttack);
                PlayerOnTurn(playerForDefend.Name + " is on turn!");
            }
        }

        internal void DefendPoint(List<int> l)
        {
            playerForAttack.GameBoard[l[0], l[1]].Defended = true;
            if(!playerForAttack.HasFlags())
               PlayerWon(playerForDefend);
        }

    }
}
