﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CiFlaG
{
    interface IPlayer
    {
        event Action<string> DispInfo;
        GameBoard GameBoard { get; set;}
        string Name { get; }
        bool Attack(int x, int y, IPlayer opponent);
        bool Defend(int x, int y);
        bool HasFlags();
    }
}
