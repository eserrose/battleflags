﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CiFlaG
{
    sealed class GameBoard : IEnumerable
    {
        //PictureBox map = Application.OpenForms["Form1"].Controls["mapImg"] as PictureBox;
        private BasicField[,] board;
        public BasicField this[int x, int y]
        {
            get
            {
                if (IsInRange(x, y))
                    return board[x, y];
                else throw new IndexOutOfRangeException();
            }
            set
            {
                if (IsInRange(x, y))
                    board[x, y] = value;
                else throw new IndexOutOfRangeException();
            }
        }

        public int GetMaxX
        {
            get { return board.GetLength(0); }
        }

        public int GetMaxY
        {
            get { return board.GetLength(1); }
        }

        public GameBoard(int x, int y)
        {
            board = new BasicField[x, y];
            InitBoard();
        }

        private void InitBoard()
        {
            for (int x = 0; x < GetMaxX; x++)
                for (int y = 0; y < GetMaxY; y++)
                    board[x, y] = new BasicField();
        }

        public void AddFlag(int x, int y)
        {
            if (!IsFlag(x, y) && IsInRange(x, y))
                board[x , y] = new Flag();
        }

        public bool IsFlag(int x, int y)
        {
            return (board[x, y] is IFlag);
        }

        private bool IsInRange(int x, int y)
        {
            return (x >= 0 &&
                y >= 0 &&
                x < board.GetLength(0) &&
                y < board.GetLength(1));
        }

        public bool IsAttacked(int x, int y)
        {
            if (!IsInRange(x, y))
                throw new IndexOutOfRangeException();
            return this[x, y].Attacked;
        }

        public bool IsDefended(int x, int y)
        {
            if (!IsInRange(x, y))
                throw new IndexOutOfRangeException();
            return this[x, y].Defended;
        }

        public IEnumerator GetEnumerator()
        {
            return board.GetEnumerator();
        }

    }
}
