﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CiFlaG
{
    interface IField
    {
        bool Attacked { get; set; }
        bool Defended { get; set; }
        void Draw(Graphics g, Point p, Size s);
    }
}
